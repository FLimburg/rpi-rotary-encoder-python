# Class to monitor a rotary encoder and update a value.  You can either read the value when you need it, by calling getValue(), or
# you can configure a callback which will be called whenever the value changes.
# Range for possible values and step size can be defined.
# Activate wrap to go from max to min+1 or from min to max-1 value

class Encoder:

    # wrap=True requires min and max
    # start must fit with min and max
    def __init__(self,
    leftPin, rightPin, switchPin=None,
    callback=None, switchCallback=None,
    minimum=None, maximum=None, start=0,
    wrap=False, simple=False, step=1):
        # parameter validation
        if wrap:
            if minimum is None or maximum is None:
                raise ValueError
        if minimum is not None and start < minimum:
                raise ValueError
        if maximum is not None and start > maximum:
                raise ValueError

        self.leftPin = leftPin
        self.rightPin = rightPin
        self.swPin = switchPin
        self.min = minimum
        self.max = maximum
        self.value = start
        self.wrap = wrap
        self.simple = simple
        if not self.simple:
            if self.wrap:
                self.trigger = self.triggerWrap
            else:
                self.trigger = self.triggerNoWrap
        else:
            self.trigger = self.triggerSimple
        self.step = step
        self.state = '00'
        self.direction = None
        self.callback = callback
        self.switchCallback = switchCallback 

        global GPIO
        try:
            tmp = __import__('RPi.GPIO', globals(), locals())
            GPIO = tmp.GPIO
        except:
            print("Mocking GPIO for testing")
            GPIO = __import__('gpio_mock', globals(), locals())

        GPIO.setup(self.leftPin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.rightPin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(self.leftPin, GPIO.BOTH, callback=self.transitionOccurred)
        GPIO.add_event_detect(self.rightPin, GPIO.BOTH, callback=self.transitionOccurred)

        if self.swPin is not None:
            GPIO.setup(self.swPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.add_event_detect(self.swPin, GPIO.FALLING, callback=self.switchPressed, bouncetime=300)


    def switchPressed(self, channel):
        if self.switchCallback is not None:
            self.switchCallback()


    def transitionOccurred(self, channel):
        p1 = GPIO.input(self.leftPin)
        p2 = GPIO.input(self.rightPin)
        newState = "{}{}".format(p1, p2)

        if self.state == "00": # Resting position
            if newState == "01": # Turned right 1
                self.direction = "R"
            elif newState == "10": # Turned left 1
                self.direction = "L"

        elif self.state == "01": # R1 or L3 position
            if newState == "11": # Turned right 1
                self.direction = "R"
            elif newState == "00": # Turned left 1
                if self.direction == "L":
                    self.trigger(-1)

        elif self.state == "10": # R3 or L1
            if newState == "11": # Turned left 1
                self.direction = "L"
            elif newState == "00": # Turned right 1
                if self.direction == "R":
                    self.trigger(+1)

        else: # self.state == "11"
            if newState == "01": # Turned left 1
                self.direction = "L"
            elif newState == "10": # Turned right 1
                self.direction = "R"
            elif newState == "00": # Skipped an intermediate 01 or 10 state, but if we know direction then a turn is complete
                if self.direction == "L":
                    self.trigger(-1)
                elif self.direction == "R":
                    self.trigger(+1)

        self.state = newState


    def setStep(step):
        self.step = step

    def triggerSimple(self, delta):
        self.callback(delta)

    def triggerWrap(self, delta):
        newValue = self.value + self.step * delta
        if newValue > self.max:
            self.value = self.min + (newValue - self.max)
        elif newValue < self.min:
            self.value = self.max + (newValue - self.min)
        else:
            self.value = newValue
        if self.callback is not None:
             self.callback(self.value)

    def triggerNoWrap(self, delta):
        newValue = self.value + self.step * delta
        if self.min is not None:
            newValue = max(self.min, newValue)
        if self.max is not None:
            newValue = min(self.max, newValue)
        self.value = newValue
        if self.callback is not None:
             self.callback(self.value)


    def getValue(self):
        return self.value

    def setValue(self, value):
        if (self.max is not None and value > self.max) \
        or (self.min is not None and value < self.min):
                return
        self.value = value
