# Sort of Unit Test framework and tests.

from encoder import Encoder
import gpio_mock as GPIO
import inspect
import math

errCounter = 0
tstCounter = 0
triggerCounter = 0

def fullCycleRight(enc, turns=1):
    for i in range(turns):
        GPIO.setPin(18,1)
        GPIO.setPin(17,1)
        GPIO.setPin(18,0)
        GPIO.setPin(17,0)

def fullCycleLeft(enc, turns=1):
    for i in range(turns):
        GPIO.setPin(17,1)
        GPIO.setPin(18,1)
        GPIO.setPin(17,0)
        GPIO.setPin(18,0)

def catchSwitch():
    global triggerCounter
    triggerCounter += 1

def verify(enc, val):
    global errCounter
    global tstCounter
    tstCounter += 1
    if enc is not None:
        testVal = enc.getValue()
    else:
        testVal = triggerCounter
    if not math.isclose(testVal, val):
        errCounter += 1
        frame = inspect.stack()[1][0]
        info = inspect.getframeinfo(frame)
        print("ERROR in function ", info.function, "at line", info.lineno, ":", testVal, "instead of", val)


def testBase():
    e = Encoder(17, 18)
    fullCycleRight(e)
    verify(e, 1,)
    fullCycleRight(e)
    verify(e, 2)
    fullCycleLeft(e)
    verify(e, 1)
    fullCycleLeft(e)
    verify(e, 0)
    fullCycleRight(e, 50)
    verify(e, 50)

def testMin():
    e2 = Encoder(17, 18, maximum=25)
    fullCycleRight(e2, 50)
    verify(e2, 25)

def testMax():
    e3 = Encoder(17, 18, minimum=-25)
    fullCycleLeft(e3, 50)
    verify(e3, -25)

def testMinMax():
    e4 = Encoder(17, 18, minimum=-25, maximum=25)
    fullCycleRight(e4, 30)
    verify(e4, 25)
    fullCycleLeft(e4, 30)
    verify(e4, -5)
    fullCycleLeft(e4, 50)
    verify(e4, -25)

def testWrap():
    e5 = Encoder(17, 18, minimum=-10, maximum=10, wrap=True)
    fullCycleRight(e5, 15)
    verify(e5, -5)
    fullCycleLeft(e5, 15)
    verify(e5, 0)

def testStep():
    e = Encoder(17, 18, step=10.1, start=0.5)
    fullCycleRight(e)
    verify(e, 10.6,)
    fullCycleRight(e)
    verify(e, 20.7)
    fullCycleLeft(e)
    verify(e, 10.6)
    fullCycleLeft(e)
    verify(e, 0.5)
    fullCycleRight(e, 50)
    verify(e, 505.5)

def testSwitch():
    e = Encoder(5,6,switchPin=13, switchCallback=catchSwitch)
    for i in range(10):
        GPIO.setPin(13, 1)
        GPIO.setPin(13, 0)
    verify(None, 10)

def testSetValue():
    e = Encoder(5,6, minimum=-10, maximum=10)
    e.setValue(5)
    verify(e, 5)
    e.setValue(-5)
    verify(e, -5)
    e.setValue(150)
    verify(e, -5)
    e.setValue(0)
    verify(e, 0)
    e.setValue(-15)
    verify(e, 0)

if __name__ == "__main__":
    testBase()
    testMin()
    testMax()
    testMinMax()
    testWrap()
    testStep()
    testSwitch()
    testSetValue()

    print("Ran {} tests. {} failed.".format(tstCounter, errCounter))
    if errCounter > 0:
        exit(-1)
