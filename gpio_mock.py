# GPIO mock for testing

IN = "IN"
OUT = "OUT"
PUD_DOWN = 0
PUD_UP = 1
BOTH = "BOTH"
FALLING = "FALLING"
RAISING = "RAISING"
pins = {}
BCM = "BCM"

def setup(pin, inOut, pull_up_down):
    pins[pin] = {"val":pull_up_down, "i/o":inOut, "pud":pull_up_down, "callback":None, "bounce":0, "flank":RAISING}

def setPin(pin, val):
    if pins[pin]["val"] != val:
        pins[pin]["val"] = val
        if (pins[pin]["callback"] is not None) \
        and ((pins[pin]["flank"] is BOTH) \
        or (pins[pin]["flank"] is RAISING and val == 1) \
        or (pins[pin]["flank"] is FALLING and val == 0)):
            pins[pin]["callback"](None)

def input(pin):
    return pins[pin]["val"]

def add_event_detect(pin, flank, callback=None, bouncetime=0):
    pins[pin]["flank"] = flank
    pins[pin]["callback"] = callback
    pins[pin]["bounce"] = bouncetime

def setmode(val):
    print("GPIO mock setmode to {}".format(val))

def cleanup():
    print("GPIO mock cleanup")