# Sample code to demonstrate Encoder class.  Prints the value every 5 seconds, and also whenever it changes.
# To exit press switch on value 0

import time
import RPi.GPIO as GPIO
from encoder import Encoder

def valueChanged(value):
    print("* New value: {}".format(value))

def switchPressed():
    global keepGoing
    print("Switch on",e1.getValue())
    if e1.getValue() == 0:
        keepGoing = False

if __name__ == "__main__":
    GPIO.setmode(GPIO.BCM)
    global keepGoing
    global e1
    keepGoing = True
    e1 = Encoder(17, 27, switchPin=22, \
    minimum=-180, maximum=180, step=10, \
    switchCallback=switchPressed, \
    callback=valueChanged, wrap=True)

    try:
        while keepGoing:
            time.sleep(5)
            print("Value is {}".format(e1.getValue()))
    except Exception:
        pass

    GPIO.cleanup()
